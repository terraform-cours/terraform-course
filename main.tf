terraform {
    required_providers {
      aws = {
        source = "hashicorp/aws"
        version = "~>3.0"
      }
    }
}

provider "aws" {
  region = "eu-west-1"
}

resource "aws_instance" "example" {
  ami = "ami-02c8058ccc9fdf64f"
  instance_type = "t2.micro"
}